#!/usr/bin/env python3

# Table of versions to download.
VERSIONS = (
    (5, 12, 9), (5, 12, 10), (5, 12, 11), (5, 12, 12),
    (5, 13, 2),
    (5, 14, 2),
    (5, 15, 0), (5, 15, 1), (5, 15, 2),
    (6, 0, 1), (6, 0, 2), (6, 0, 3), (6, 0, 4),
    (6, 1, 0), (6, 1, 1), (6, 1, 2), (6, 1, 3),
    (6, 2, 0), (6, 2, 1), (6, 2, 2), (6, 2, 3), (6, 2, 4),
    (6, 3, 0), (6, 3, 1), (6, 3, 2),
    (6, 4, 0), (6, 4, 1), (6, 4, 2), (6, 4, 3),
    (6, 5, 0), (6, 5, 1), (6, 5, 2), (6, 5, 3),
    (6, 6, 0), (6, 6, 1), (6, 6, 2), (6, 6, 3),
    (6, 7, 0), (6, 7, 1), (6, 7, 2), (6, 7, 3),
    (6, 8, 0), (6, 8, 1), (6, 8, 2),
    (6, 9, 0),
)

# Placeholder version constants.
VERS_ALL = (0, 0, 0)
VERS_ALWAYS = (9999, 0, 0)
VERS_QT6 = (6, 0, 0)

# There's a new download layout starting with this version.
VERS_NEW_LAYOUT = (6, 8, 0)

# Table of platforms and version description.
PLATFORMS = {
    'linux_arm64': {
        'since': (6, 7, 0),
        'abi': 'linux_gcc_arm64',
    },
    'linux_x64': {
        'since': VERS_ALL,
        'abi': {
            'gcc_64': (VERS_ALL, (6, 7, 0)),
            'linux_gcc_64': ((6, 7, 0), VERS_ALWAYS),
        },
    },

    'mac_x64': {
        'since': VERS_ALL,
        'abi': 'clang_64',
    },

    'windows_arm64': {
        'since': (6, 8, 0),
        'abi': 'win64_msvc2022_arm64',
    },
    'windows_x86': {
        'since': VERS_ALL,
        'abi': {
            'win64_msvc2015_64': (VERS_ALL, VERS_QT6),
            'win64_msvc2017_64': (VERS_ALL, (5, 15, 1)),
            'win64_msvc2019_64': ((5, 15, 1), VERS_NEW_LAYOUT),
            'win64_msvc2019_arm64': ((6, 2, 0), VERS_NEW_LAYOUT),
            'win64_msvc2022_64': (VERS_NEW_LAYOUT, VERS_ALWAYS),
            'win64_msvc2022_arm64_cross_compiled': (VERS_NEW_LAYOUT, VERS_ALWAYS),
        },
    },
}

ADDONS = {
    5: {},
    6: {
        'qt5compat': (VERS_QT6, VERS_NEW_LAYOUT, {}),
        'addons.qt5compat': (VERS_NEW_LAYOUT, VERS_ALWAYS, {}),
        'addons.qtmultimedia': ((6, 2, 0), VERS_ALWAYS, {}),
        'addons.qtwebchannel': ((6, 2, 0), VERS_ALWAYS, {}),
        'addons.qtwebengine': ((6, 2, 0), (6, 8, 0), {
            'exclude': (
                'windows_x86/win64_msvc2019_arm64',
            ),
        }),
        'addons.qtwebsockets': ((6, 2, 0), VERS_ALWAYS, {}),
        'addons.qtwebview': ((6, 2, 0), VERS_ALWAYS, {
            'exclude': (
                'windows_arm64/win64_msvc2022_arm64',
                'windows_x86/win64_msvc2019_arm64',
                'windows_x86/win64_msvc2022_arm64_cross_compiled',
            ),
        }),
    },
}

# Where to download files from.
URL_PREFIX = 'https://download.qt.io/online/qtsdkrepository'


for platform, description in PLATFORMS.items():
    since = description['since']
    abi = description['abi']

    # Build a default ABI description from the simple case.
    if type(abi) == str:
        abi = {
            abi: (VERS_ALL, VERS_ALWAYS),
        }

    for version in VERSIONS:
        if version < since:
            continue

        major, minor, patch = version

        for abi_name, (abi_since, abi_until) in abi.items():
            if version < abi_since:
                continue
            if abi_until <= version:
                continue

            vers_dir = f'qt{major}_{major}{minor}{patch}'
            if version < VERS_NEW_LAYOUT:
                vers_prefix = f'{URL_PREFIX}/{platform}/desktop/{vers_dir}'
            else:
                vers_prefix = f'{URL_PREFIX}/{platform}/desktop/{vers_dir}/{vers_dir}'
            print(f'{vers_prefix}/qt.qt{major}.{major}{minor}{patch}.{abi_name}/')

            platform_abi = f'{platform}/{abi_name}'
            for addon, (addon_since, addon_until, props) in ADDONS[major].items():
                if version < addon_since:
                    continue
                if addon_until <= version:
                    continue

                if platform_abi in props.get('exclude', []):
                    continue

                print(f'{vers_prefix}/qt.qt{major}.{major}{minor}{patch}.{addon}.{abi_name}/')
