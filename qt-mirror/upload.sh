#!/bin/sh

# Add a stanza like the following to your `~/.ssh/config`. You'll need to send
# an appropriate (new) SSH key to sysadmin@ asking for access to
# `ParaView_internal_deps`.

# Host paraview.dependencies.internal
#     User         kitware
#     HostName     web.kitware.com
#     IdentityFile ~/.local/share/ssh/kitware-paraview-deps-internal
#     IdentitiesOnly  yes

exec rsync -rptv --progress download.qt.io/online/qtsdkrepository/ paraview.dependencies.internal:ParaView_internal_deps/qt/
