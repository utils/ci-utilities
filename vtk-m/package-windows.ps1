$erroractionpreference = "stop"

# Keep in sync with the MacOS script.
$git_url = 'https://gitlab.kitware.com/vtk/vtk-m.git'
$git_tag = 'v2.1.0'

git clone "$git_url" vtkm/src
git -C vtkm/src -c advice.detachedHead=false checkout "$git_tag"

$here = pwd

New-Item -Path "vtkm\build" -Type Directory
cd "vtkm\build"
cmake `
  -GNinja `
  -DCMAKE_BUILD_TYPE=Release `
  -DBUILD_SHARED_LIBS=ON `
  -DBUILD_TESTING=OFF `
  -DVTKm_ENABLE_TESTING=OFF `
  -DCMAKE_INSTALL_PREFIX="$here\install\vtkm-$git_tag-windows-x86_64" `
  "..\src"
ninja
ninja install
cd ..\..

# create final zip
Compress-Archive -LiteralPath "$here\install\vtkm-$git_tag-windows-x86_64" -DestinationPath "$here\vtkm-$git_tag-windows-x86_64.zip"
