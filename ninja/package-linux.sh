#!/bin/sh

# Run this script on an Alpine Linux host to generate ninja binaries.

set -e
set -x

umask 022

. ninja/package-common.sh

apk update
apk add cmake make gcc g++

arch="$( uname -m )"
readonly arch

mkdir "ninja-$arch"
cd "ninja-$arch"
cmake \
  -DCMAKE_BUILD_TYPE=Release \
  -DBUILD_TESTING=OFF \
  -DCMAKE_EXE_LINKER_FLAGS="-static-libstdc++ -static" \
  "../ninja/src"
make "-j$(nproc)"
cd ..

mv ninja-$arch/ninja "ninja-$version-linux-$arch"
strip "ninja-$version-linux-$arch"
