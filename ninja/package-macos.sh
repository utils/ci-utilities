#!/bin/sh

# Run this script on a macOS host to generate universal ninja binaries.

set -e
set -x

umask 022

. ninja/package-common.sh

readonly cmake_version="3.19.3"
readonly cmake_sha256sum="a6b79ad05f89241a05797510e650354d74ff72cc988981cdd1eb2b3b2bda66ac"

readonly cmake_filename="cmake-$cmake_version-macos-universal"
readonly cmake_tarball="$cmake_filename.tar.gz"

echo "$cmake_sha256sum  $cmake_tarball" > cmake.sha256sum
curl -OL "https://github.com/Kitware/CMake/releases/download/v$cmake_version/$cmake_tarball"
shasum -a 256 --check cmake.sha256sum
tar xf "$cmake_tarball"
mv "$cmake_filename" cmake

PATH="$PWD/cmake/CMake.app/Contents/bin:$PATH"

readonly macos_x86_64_target="10.13"
readonly macos_arm64_target="11.0"

# Build the x86_64 variant.
mkdir "ninja-x86_64"
cd "ninja-x86_64"
cmake \
  -DCMAKE_BUILD_TYPE=Release \
  -DBUILD_TESTING=OFF \
  -DCMAKE_OSX_DEPLOYMENT_TARGET="$macos_x86_64_target" \
  -DCMAKE_OSX_ARCHITECTURES="x86_64" \
  "../ninja/src"
make "-j$( sysctl -n hw.ncpu )"
cd ..

# Build the arm64 variant.
mkdir "ninja-arm64"
cd "ninja-arm64"
cmake \
  -DCMAKE_BUILD_TYPE=Release \
  -DBUILD_TESTING=OFF \
  -DCMAKE_OSX_DEPLOYMENT_TARGET="$macos_arm64_target" \
  -DCMAKE_OSX_ARCHITECTURES="arm64" \
  "../ninja/src"
make "-j$( sysctl -n hw.ncpu )"
cd ..

lipo -create \
  -output "ninja-$version-macos-universal" \
  "ninja-arm64/ninja" \
  "ninja-x86_64/ninja"
