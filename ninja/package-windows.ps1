$erroractionpreference = "stop"

# Keep in sync with the common script.
$git_url = 'https://github.com/Kitware/ninja.git'
$git_commit = '95dee2a91d96c409d54f9fa0b70ea9aa2bdf8e63' # v1.11.1.g95dee.kitware.jobserver-1
$version = '1.11.1.g95dee.kitware.jobserver-1'

git clone "$git_url" ninja/src
git -C ninja/src -c advice.detachedHead=false checkout "$git_commit"

if ($env:VSCMD_ARG_TGT_ARCH -eq "x64") {
  $arch = "x86_64";
} elseif ($env:VSCMD_ARG_TGT_ARCH -eq "arm64") {
  $arch = "arm64";
} else {
  Write-Host "VSCMD_ARG_TGT_ARCH env var not recognized.  Run this from a Visual Studio Command Prompt."
  exit 1
}

New-Item -Path "ninja-$arch" -Type Directory
cd "ninja-$arch"
cmake `
  -GNinja `
  -DCMAKE_BUILD_TYPE=Release `
  -DBUILD_TESTING=OFF `
  "../ninja/src"
cmake --build .
cd ..

mv ninja-$arch/ninja.exe "ninja-$version-windows-$arch.exe"
