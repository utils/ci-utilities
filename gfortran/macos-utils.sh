download_verify_extract () {
    name="$1"
    shift

    url="$1"
    shift

    sha256sum="$1"
    shift

    filename="$1"
    shift

    curl -OL "$url"
    echo "$sha256sum  $filename" > "$name.sha256sum"
    shasum -a 256 --check "$name.sha256sum"

    mkdir -p "$name/src"
    tar -C "$name/src" --strip-components=1 -xf "$filename"
}

build () {
    name="$1"
    shift

    prefix="$1"
    shift

    destination="$1"
    shift

    mkdir -p "$name/build"
    cd "$name/build" || return 1

    ../src/configure \
        "--prefix=$prefix" \
        "$@"
    make "-j$( sysctl -n hw.ncpu )"
    make "DESTDIR=$destination" install

    cd ../.. || return 1
}
