#!/bin/bash

set -e

readonly git_url='https://gitlab.kitware.com/utils/ghostflow-director.git'
readonly git_commit='9ae8db48c20f4edb5037161ba211c1625f68e83c' # ghostflow-pipeline-state-fix
readonly cargo_features="--features systemd"

git clone "$git_url" ghostflow-director/src
pushd ghostflow-director/src
git -c advice.detachedHead=false checkout "$git_commit"
short_commit="$( git rev-parse --short "$git_commit" )"
readonly short_commit
cargo build $cargo_features
cargo run $cargo_features -- --version
popd
mv ghostflow-director/src/target/debug/ghostflow-director "ghostflow-director-$short_commit"
