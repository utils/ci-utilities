#!/bin/sh

set -e

# The version of Git to build.
readonly version="2.48.1"

# Target macOS version.
readonly macos_target="14"
readonly macos_arches="x86_64 arm64"

macos_arch_flags=""
for macos_arch in $macos_arches; do
    macos_arch_flags="$macos_arch_flags -arch $macos_arch"
done
readonly macos_arch_flags

readonly gitsrcdir="work/git"

# Clone the repository.
git clone --depth 1 --branch "v$version" https://github.com/gitster/git.git "$gitsrcdir"

# Configure our options.
cat > "$gitsrcdir/config.mak" <<EOF
# We deploy to /usr/local.
prefix=/usr/local

# We support macOS 14+ on our CI machines.
CFLAGS=-mmacosx-version-min=$macos_target $macos_arch_flags

# Support looking up the prefix at runtime.
RUNTIME_PREFIX=1

# Avoid duplicating files upon extraction.
INSTALL_SYMLINKS=1

# Disable unnecessary features.
NO_GETTEXT=1
NO_TCLTK=1
NO_PERL=1
EOF

# Build and install Git.
nproc="$( sysctl -n hw.ncpu )"
readonly nproc
readonly destdir="git-$version"

make -C "$gitsrcdir" "-j$nproc"
make -C "$gitsrcdir" "DESTDIR=$PWD/$destdir" install

# Prepare into our deployment bundle.
readonly tree_tarball="git-$version.tar.bz2"
readonly manifest="manifest.txt"
readonly deploy_script_name="macos-ci-deploy.sh"
readonly deploy_script="git/$deploy_script_name.in"

# Create the tarball.
tar --uid 0 --gid 0 -cjf "$tree_tarball" "$destdir"
# Create the manifest of the files (ignoring directories).
tar tf "$tree_tarball" \
    | sed -e "s,$destdir,," -e '/\/$/d' \
    > "$manifest"
# Create the deployment script.
sed -e "s,VERSION,$version,g" "$deploy_script" \
    > "$deploy_script_name"
chmod +x "$deploy_script_name"

tar --uid 0 --gid 0 -cf "kitware-ci-macos-git-$version.tar" \
    "$tree_tarball" \
    "$manifest" \
    "$deploy_script_name"
