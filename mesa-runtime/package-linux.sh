#!/bin/sh

set -e

readonly git_url='https://gitlab.kitware.com/paraview/common-superbuild'
readonly git_commit='ad2b6c0444299984dea21b767004e5bf2e84884b'

mkdir -p mesa-runtime
cd mesa-runtime
git clone "$git_url" src
git -C src checkout "$git_commit"
mkdir build
cd build
cmake \
  -G Ninja \
  -Dllvm_BUILD_SHARED_LIBS:STRING=OFF \
  -DSUPPRESS_bzip2_OUTPUT:BOOL=ON \
  -DSUPPRESS_expat_OUTPUT:BOOL=ON \
  -DSUPPRESS_ffi_OUTPUT:BOOL=ON \
  -DSUPPRESS_glproto_OUTPUT:BOOL=ON \
  -DSUPPRESS_llvm_OUTPUT:BOOL=ON \
  -DSUPPRESS_mesa_OUTPUT:BOOL=ON \
  -DSUPPRESS_meson_OUTPUT:BOOL=ON \
  -DSUPPRESS_ninja_OUTPUT:BOOL=ON \
  -DSUPPRESS_png_OUTPUT:BOOL=ON \
  -DSUPPRESS_python3_OUTPUT:BOOL=ON \
  -DSUPPRESS_pythonmako_OUTPUT:BOOL=ON \
  -DSUPPRESS_pythonsetuptools_OUTPUT:BOOL=ON \
  -DSUPPRESS_sqlite_OUTPUT:BOOL=ON \
  -DSUPPRESS_xz_OUTPUT:BOOL=ON \
  -DSUPPRESS_zlib_OUTPUT:BOOL=ON \
  ../src/standalone-mesa
ninja
ctest -R cpack-mesa -VV
