#!/bin/sh

set -e

readonly git_lfs_version="3.6.1"
readonly git_lfs_amd64_sha256="b53c361e6c85479507ed39ba99b87ec0888ac52f5afd2084fc68af4103081391"
readonly git_lfs_arm64_sha256="83b4ea3b0c72ba19e3bc46e47e92476f4505cc96693333b9fa0a314dddacc4ba"

readonly git_lfs_amd64_url="https://github.com/git-lfs/git-lfs/releases/download/v$git_lfs_version/git-lfs-darwin-amd64-v$git_lfs_version.zip"
readonly git_lfs_arm64_url="https://github.com/git-lfs/git-lfs/releases/download/v$git_lfs_version/git-lfs-darwin-arm64-v$git_lfs_version.zip"

cat > git-lfs.sha256sum <<EOF
$git_lfs_amd64_sha256  git-lfs-darwin-amd64-v$git_lfs_version.zip
$git_lfs_arm64_sha256  git-lfs-darwin-arm64-v$git_lfs_version.zip
EOF
curl -OL "$git_lfs_amd64_url"
curl -OL "$git_lfs_arm64_url"
shasum -a 256 --check git-lfs.sha256sum

mkdir amd64
unzip -d amd64 "git-lfs-darwin-amd64-v$git_lfs_version.zip"
mkdir arm64
unzip -d arm64 "git-lfs-darwin-arm64-v$git_lfs_version.zip"

lipo -create \
    -output "git-lfs-darwin-universal-$git_lfs_version" \
    "amd64/git-lfs-$git_lfs_version/git-lfs" \
    "arm64/git-lfs-$git_lfs_version/git-lfs"
