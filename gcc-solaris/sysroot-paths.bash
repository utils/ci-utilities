#!/usr/bin/env bash

set -e

# Solaris package manifests are available here:
#     http://pkg.oracle.com/solaris/release/en/catalog.shtml
pkg_fmri_urls='
  http://pkg.oracle.com/solaris/release/manifest/0/system/header@0.5.11,5.11-0.175.3.1.0.3.0:20150925T164321Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/header@11.4,5.11-11.4.0.0.1.15.0:20180817T003001Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/header@11.4,5.11-11.4.42.0.0.111.1:20211203T213657Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/library@11.4,5.11-11.4.0.0.1.15.0:20180817T003213Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/library@11.4,5.11-11.4.42.0.0.111.1:20211203T214210Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/library/libc@11.4-11.4.42.0.0.111.1:20211203T214042Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/library/libc@11.4,5.11-11.4.42.0.0.111.1:20211203T214042Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/library/math@11.4-11.4.0.1.0.17.0:20200221T005425Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/library/math@11.4,5.11-11.4.0.1.0.17.0:20200221T005425Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/library/security/crypto@11.4,5.11-11.4.42.0.0.111.1:20211203T214125Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/linker@11.4-11.4.0.0.1.15.0:20180817T003258Z
  http://pkg.oracle.com/solaris/release/manifest/0/system/linker@11.4,5.11-11.4.42.0.0.111.1:20211203T214307Z
  http://pkg.oracle.com/solaris/release/manifest/0/library/zlib@1.2.11,11.4-11.4.42.0.0.111.0:20211203T203816Z
  http://pkg.oracle.com/solaris/release/manifest/0/library/zlib@1.2.11,5.11-11.4.0.0.1.14.0:20180814T170708Z
'

# Fetch package manifests and parse out a list of relevant paths.
curl -sS $pkg_fmri_urls |
  grep -hE '^(file|link) ' |
  grep -o 'path=[^ ]*' |
  sed 's|^path=||' |
  sort -u |
  grep -E -f "${BASH_SOURCE%/*}/sysroot-paths.include" |
  grep -E -f "${BASH_SOURCE%/*}/sysroot-paths.exclude" -v
