$erroractionpreference = "stop"

# Keep in sync with the common script.
$git_url = 'https://github.com/lexxmark/winflexbison'
$git_commit = '300f48ba3e598e7a6600d724b58541fbcc3c2fa6' # PR #91 merged
$version = '2.5.25.g300f48b-concurrent-builds'

git clone "$git_url" winflexbison/src
git -C winflexbison/src -c advice.detachedHead=false checkout "$git_commit"

if ($env:VSCMD_ARG_TGT_ARCH -eq "x64") {
  $arch = "x86_64";
} elseif ($env:VSCMD_ARG_TGT_ARCH -eq "arm64") {
  $arch = "arm64";
} else {
  Write-Host "VSCMD_ARG_TGT_ARCH env var not recognized.  Run this from a Visual Studio Command Prompt."
  exit 1
}

New-Item -Path "winflexbison-$arch" -Type Directory
cd "winflexbison-$arch"
cmake `
  -GNinja `
  -DCMAKE_BUILD_TYPE=Release `
  -DBUILD_TESTING=OFF `
  "../winflexbison/src"
cmake --build .
cpack -G ZIP
cd ..

mv winflexbison-$arch/win_flex_bison-master.zip "winflexbison-$version-windows-$arch.zip"
