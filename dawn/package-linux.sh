#!/bin/sh

# Run this script on a linux host to generate dawn binaries.

set -e
set -x

target_arch="$( uname -m )"
case "$target_arch" in
    x86_64)
        target="linux-$target_arch"
        ;;
    *)
        echo >&2 "Unknown architecture: $target_arch"
        exit 1
        ;;
esac
readonly target

# Keep in sync with the Windows script.
readonly git_url="https://dawn.googlesource.com/dawn"
# Dawn doesn't have any version tags. Instead the tags refer to chromium's patch version.
readonly chromium_patch_version="7037"
readonly git_tag="chromium/$chromium_patch_version"

git clone --depth=1 --branch "$git_tag" "$git_url" dawn/src

here="$( pwd )"
readonly here

cmake \
  -GNinja \
  -C "dawn/dawn.cmake" \
  -S "dawn/src" \
  -B "dawn/build"
cmake --build "dawn/build"
cmake --install "dawn/build" --prefix "$here/install/dawn-$chromium_patch_version-$target"

# Create the final tarball containing the binaries.
tar cvzf \
  "dawn-$chromium_patch_version-$target.tar.gz" \
  -C "$here/install" \
  "dawn-$chromium_patch_version-$target"
