# Install toolchain dependencies.
apk update
apk add gcc musl-dev

# Add support for needed architectures.
rustup target add aarch64-unknown-linux-musl

# Report result for human reference.
rustc --version
