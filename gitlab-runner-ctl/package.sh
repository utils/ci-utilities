#!/bin/bash

set -e

readonly git_url='https://gitlab.kitware.com/utils/gitlab-runner-ctl.git'
readonly git_commit='eb58f890308e344d1db89c651870d544f5de7740' # filter-maint-note-contents

git clone "$git_url" gitlab-runner-ctl/src
pushd gitlab-runner-ctl/src
git -c advice.detachedHead=false checkout "$git_commit"
short_commit="$( git rev-parse --short "$git_commit" )"
readonly short_commit
cargo build
cargo run -- --version
popd
mv gitlab-runner-ctl/src/target/debug/gitlab-runner-ctl "gitlab-runner-ctl-$short_commit"
