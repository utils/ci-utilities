$erroractionpreference = "stop"

$git_url = 'https://gitlab.kitware.com/paraview/common-superbuild.git'
$git_commit = '6a72b3932bf14e64b1a46724b1e1d468134dd8a8'

git clone "$git_url" osmesa-sdk/src
git -C osmesa-sdk/src checkout "$git_commit"

New-Item -Path "osmesa-sdk\build" -Type Directory

cd "osmesa-sdk\build"
cmake `
  -G Ninja `
  -DCMAKE_BUILD_TYPE:STRING=Release `
  -DSUPPRESS_cxx17_OUTPUT:BOOL=ON `
  -DSUPPRESS_flexbison_OUTPUT:BOOL=ON `
  -DSUPPRESS_llvm_OUTPUT:BOOL=ON `
  -DSUPPRESS_meson_OUTPUT:BOOL=ON `
  -DSUPPRESS_ninja_OUTPUT:BOOL=ON `
  -DSUPPRESS_osmesa_OUTPUT:BOOL=ON `
  -DSUPPRESS_python3_OUTPUT:BOOL=ON `
  -DSUPPRESS_pythonmako_OUTPUT:BOOL=ON `
  -DSUPPRESS_pythonsetuptools_OUTPUT:BOOL=ON `
  -DSUPPRESS_pywin32_OUTPUT:BOOL=ON `
  -DSUPPRESS_zlib_OUTPUT:BOOL=ON `
  ..\src\standalone-mesa
ninja
ctest -R cpack-osmesa -VV
