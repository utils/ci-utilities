#!/bin/sh

set -e

readonly git_url='https://github.com/mathstuf/clang-tidy-cache'
readonly git_commit='933cbae6effa40f72bc227452cbf39d2ef7ab92c' # kitware-ci-20220709
readonly version='0.4.0-kitware-ci-20220709'

readonly build_platforms='
  linux/amd64
'

git clone "$git_url" clang-tidy-cache/src
cd clang-tidy-cache/src
git checkout "$git_commit"

for p in $build_platforms; do
  goos="${p%%/*}"
  goarch="${p##*/}"
  GOOS="$goos" GOARCH="$goarch" go build
  mv clang-tidy-cache "clang-tidy-cache-$goos-$goarch"
done

cd ../..
mkdir -p out

readonly tmp=clang-tidy-cache/src
readonly out=out/clang-tidy-cache-$version
cp -a $tmp/clang-tidy-cache-linux-amd64        $out-linux-amd64
