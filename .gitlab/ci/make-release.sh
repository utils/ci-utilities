#!/bin/sh

set -e

make_release () {
    exec release-cli create --name "Release $CI_COMMIT_TAG" --tag-name "$CI_COMMIT_TAG" "$@"
}

readonly urlbase="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_COMMIT_TAG}"

case "$CI_COMMIT_TAG" in
    bison/v*)
        # Asset discovery.
        macos_x86_64_binary="$( ls bison-*-macos-x86_64.tar.xz )"
        macos_arm64_binary="$( ls bison-*-macos-arm64.tar.xz )"
        readonly macos_x86_64_binary
        readonly macos_arm64_binary

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64\",\"url\":\"$urlbase/$macos_x86_64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS amd64\",\"url\":\"$urlbase/$macos_arm64_binary\"}"
        ;;
    clang-tidy-cache/v*)
        # Change to where the binaries live.
        cd out/

        # Asset discovery.
        linux_amd64="$( ls clang-tidy-cache-*-linux-amd64 )"
        readonly linux_amd64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux amd64\",\"url\":\"$urlbase/$linux_amd64\"}"
        ;;
    git-lfs/v*)
        # Asset discovery.
        macos_binary="$( ls git-lfs-darwin-universal-* )"
        readonly macos_binary

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS universal\",\"url\":\"$urlbase/$macos_binary\"}"
        ;;
    git-macos/v*)
        # Asset discovery.
        macos_binary="$( ls kitware-ci-macos-git-*.tar )"
        readonly macos_binary

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG deployment tarball\",\"url\":\"$urlbase/$macos_binary\"}"
        ;;
    gcc-solaris/v*)
        # Asset discovery.
        linux_x86_64_cross_sunos_i386="$( ls gcc-*-linux-x86_64-cross-sunos-i386.tar.xz )"
        linux_x86_64_cross_sunos_x86_64="$( ls gcc-*-linux-x86_64-cross-sunos-x86_64.tar.xz )"
        linux_x86_64_cross_sunos_sparc="$( ls gcc-*-linux-x86_64-cross-sunos-sparc.tar.xz )"
        linux_x86_64_cross_sunos_sparc64="$( ls gcc-*-linux-x86_64-cross-sunos-sparc64.tar.xz )"
        readonly linux_x86_64_cross_sunos_i386
        readonly linux_x86_64_cross_sunos_x86_64
        readonly linux_x86_64_cross_sunos_sparc
        readonly linux_x86_64_cross_sunos_sparc64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64 cross to SunOS i386\",\"url\":\"$urlbase/$linux_x86_64_cross_sunos_i386\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64 cross to SunOS x86_64\",\"url\":\"$urlbase/$linux_x86_64_cross_sunos_x86_64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64 cross to SunOS sparc\",\"url\":\"$urlbase/$linux_x86_64_cross_sunos_sparc\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64 cross to SunOS sparc64\",\"url\":\"$urlbase/$linux_x86_64_cross_sunos_sparc64\"}"
        ;;
    gfortran-macos/v*)
        # Asset discovery.
        macos_x86_64_binary="$( ls gcc-*-x86_64.tar.xz )"
        macos_aarch64_binary="$( ls gcc-*-aarch64.tar.xz )"
        readonly macos_x86_64_binary
        readonly macos_aarch64_binary

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64\",\"url\":\"$urlbase/$macos_x86_64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS aarch64\",\"url\":\"$urlbase/$macos_aarch64_binary\"}"
        ;;
    gitlab-runner/v*)
        # Change to where the binaries live.
        cd out/

        # Asset discovery.
        linux_amd64_deb="$( ls gitlab-runner-*-amd64.deb )"
        linux_arm64_deb="$( ls gitlab-runner-*-arm64.deb )"
        linux_amd64_rpm="$( ls gitlab-runner-*-amd64.rpm )"
        linux_arm64_rpm="$( ls gitlab-runner-*-arm64.rpm )"
        macos_amd64="$( ls gitlab-runner-*-darwin-amd64 )"
        macos_arm64="$( ls gitlab-runner-*-darwin-arm64 )"
        windows_amd64="$( ls gitlab-runner-*-windows-amd64.exe  )"
        windows_arm64="$( ls gitlab-runner-*-windows-arm64.exe  )"
        readonly linux_amd64_deb
        readonly linux_arm64_deb
        readonly linux_amd64_rpm
        readonly linux_arm64_rpm
        readonly macos_amd64
        readonly macos_arm64
        readonly windows_amd64
        readonly windows_arm64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux amd64 .deb\",\"url\":\"$urlbase/$linux_amd64_deb\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux arm64 .deb\",\"url\":\"$urlbase/$linux_arm64_deb\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux amd64 .rpm\",\"url\":\"$urlbase/$linux_amd64_rpm\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux arm64 .rpm\",\"url\":\"$urlbase/$linux_arm64_rpm\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS amd64\",\"url\":\"$urlbase/$macos_amd64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS arm64\",\"url\":\"$urlbase/$macos_arm64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows amd64\",\"url\":\"$urlbase/$windows_amd64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows arm64\",\"url\":\"$urlbase/$windows_arm64\"}"
        ;;
    sccache/v*)
        # Asset discovery.
        linux_x86_64="$( ls sccache-*-x86_64-unknown-linux-musl )"
        linux_aarch64="$( ls sccache-*-aarch64-unknown-linux-musl )"
        macos_universal="$( ls sccache-*-universal-apple-darwin )"
        windows_x86_64="$( ls sccache-*-x86_64-pc-windows-gnu.exe )"
        readonly linux_x86_64
        readonly linux_aarch64
        readonly macos_universal
        readonly windows_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux aarch64\",\"url\":\"$urlbase/$linux_aarch64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64/arm64\",\"url\":\"$urlbase/$macos_universal\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}"
        ;;
    cargo2junit/v*)
        # Asset discovery.
        linux_x86_64="$( ls cargo2junit-*-x86_64-unknown-linux-gnu )"
        readonly linux_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64\"}"
        ;;
    qt/v*)
        # Asset discovery.
        macos_arm64="$( ls qt-*-macosx*-arm64.tar.xz )"
        readonly macos_arm64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS arm64\",\"url\":\"$urlbase/$macos_arm64\"}"
        ;;
    mesa-runtime/v*)
        # Change to where the packages live.
        cd mesa-runtime/build

        # Asset discovery.
        linux_x86_64="$( ls mesa-*-linux-x86_64.tar.gz )"
        readonly linux_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64\"}"
        ;;
    osmesa-sdk/v*)
        # Change to where the packages live.
        cd osmesa-sdk/build

        # Asset discovery.
        windows_x86_64="$( ls osmesa-*-windows-AMD64.zip )"
        readonly windows_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}"
        ;;
    ninja/v*)
        # Asset discovery.
        linux_aarch64="$( ls ninja-*-linux-aarch64 )"
        linux_x86_64="$( ls ninja-*-linux-x86_64 )"
        macos_universal="$( ls ninja-*-macos-universal )"
        windows_x86_64="$( ls ninja-*-windows-x86_64.exe )"
        windows_arm64="$( ls ninja-*-windows-arm64.exe )"
        readonly linux_aarch64
        readonly linux_x86_64
        readonly macos_universal
        readonly windows_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux aarch64\",\"url\":\"$urlbase/$linux_aarch64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64/arm64\",\"url\":\"$urlbase/$macos_universal\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows arm64\",\"url\":\"$urlbase/$windows_arm64\"}"
        ;;
    ccache/v*)
        # Asset discovery.
        macos_universal="$( ls ccache-*-macos-universal )"
        readonly macos_universal

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64/arm64\",\"url\":\"$urlbase/$macos_universal\"}"
        ;;
    gitlab-runner-ctl/v*)
        # Asset discovery.
        linux_x86_64="$( ls gitlab-runner-ctl-* )"
        readonly linux_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64\"}"
        ;;
    ghostflow-director/v*)
        # Asset discovery.
        linux_x86_64="$( ls ghostflow-director-* )"
        readonly linux_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64\"}"
        ;;
    webhook-listen/v*)
        # Asset discovery.
        linux_x86_64="$( ls webhook-listen-* )"
        readonly linux_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64\"}"
        ;;
    cdi/v*)
        # Asset discovery.
        macos_x86_64_binary="$( ls cdi-*-x86_64.tar.gz )"
        macos_aarch64_binary="$( ls cdi-*-aarch64.tar.gz )"
        readonly macos_x86_64_binary
        readonly macos_aarch64_binary

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64\",\"url\":\"$urlbase/$macos_x86_64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS aarch64\",\"url\":\"$urlbase/$macos_aarch64_binary\"}"
        ;;
    adios2/v*)
        # Asset discovery.
        macos_x86_64_binary="$( ls adios2-*-x86_64.tar.gz )"
        macos_arm64_binary="$( ls adios2-*-arm64.tar.gz )"
        windows_x86_64="$( ls adios2-nompi-*-windows-x86_64.zip )"
        windows_x86_64_mpi="$( ls adios2-mpi-*-windows-x86_64.zip )"
        readonly macos_x86_64_binary
        readonly macos_arm64_binary
        readonly windows_x86_64
        readonly windows_x86_64_mpi

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64\",\"url\":\"$urlbase/$macos_x86_64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS arm64\",\"url\":\"$urlbase/$macos_arm64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64 MPI\",\"url\":\"$urlbase/$windows_x86_64_mpi\"}" \
        ;;
    vtkm/v*)
        # Asset discovery.
        macos_x86_64_binary="$( ls vtkm-*-x86_64.tar.gz )"
        macos_arm64_binary="$( ls vtkm-*-arm64.tar.gz )"
        windows_x86_64="$( ls vtkm-*-windows-x86_64.zip )"
        readonly macos_x86_64_binary
        readonly macos_arm64_binary
        readonly windows_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64\",\"url\":\"$urlbase/$macos_x86_64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS arm64\",\"url\":\"$urlbase/$macos_arm64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}" \
        ;;
    fletch-kwiver/v*)
        # Asset discovery.
        windows_x86_64="$( ls fletch-kwiver-*-windows-x86_64.zip )"
        readonly windows_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}" \
        ;;
    dawn/v*)
        # Asset discovery.
        linux_x86_64_binary="$( ls dawn-*-linux-x86_64.tar.gz )"
        macos_x86_64_binary="$( ls dawn-*-macos-x86_64.tar.gz )"
        macos_arm64_binary="$( ls dawn-*-macos-arm64.tar.gz )"
        windows_x86_64="$( ls dawn-*-windows-x86_64.zip )"
        wasm32_emscripten_binary="$( ls dawn-*-wasm32-emscripten.tar.gz )"
        wasm64_emscripten_binary="$( ls dawn-*-wasm64-emscripten.tar.gz )"
        readonly linux_x86_64_binary
        readonly macos_x86_64_binary
        readonly macos_arm64_binary
        readonly windows_x86_64
        readonly wasm32_emscripten_binary
        readonly wasm64_emscripten_binary

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Linux x86_64\",\"url\":\"$urlbase/$linux_x86_64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS x86_64\",\"url\":\"$urlbase/$macos_x86_64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG macOS arm64\",\"url\":\"$urlbase/$macos_arm64_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG wasm32\",\"url\":\"$urlbase/$wasm32_emscripten_binary\"}" \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG wasm64\",\"url\":\"$urlbase/$wasm64_emscripten_binary\"}"
        ;;
    winflexbison/v*)
        # Asset discovery.
        windows_x86_64="$( ls winflexbison-*-windows-x86_64.zip )"
        readonly windows_x86_64

        make_release \
            --assets-link "{\"name\":\"$CI_COMMIT_TAG Windows x86_64\",\"url\":\"$urlbase/$windows_x86_64\"}"
        ;;

    *)
        echo >&2 "Unknown release artifact set."
        exit 1
esac
