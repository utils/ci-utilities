cmake_minimum_required(VERSION 3.12)

set(python_url_root "https://www.paraview.org/files/dependencies/python-for-wheels")

set(python_version "3.8")
if (NOT "$ENV{PYTHON_VERSION}" STREQUAL "")
  set(python_version "$ENV{PYTHON_VERSION}")
endif ()

set("python_version_full_3.6" "3.6.8")
set("python_version_full_3.7" "3.7.9")
set("python_version_full_3.8" "3.8.10")
set("python_version_full_3.9" "3.9.13")
set("python_version_full_3.10" "3.10.11")
set("python_version_full_3.11" "3.11.6")
set("python_version_full_3.12" "3.12.0")
set(python_version_full "${python_version_full_${python_version}}")
if (NOT python_version_full)
  message(FATAL_ERROR
    "Unknown Python version ${python_version}")
endif ()

if ("$ENV{CI_JOB_NAME}" MATCHES "windows")
  set(python_subdir "python-${python_version_full}-windows-x86_64")
  set("sha256sum_3.6" "3f70798c4885d11842869c9fb2842c8f8bafed1ebfac04e3f4ae1af6498527ce")
  set("sha256sum_3.7" "edd1b8c491635939f7b7e538650607db8307d6dfd3fef043ec5bc21ce4035700")
  set("sha256sum_3.8" "d4e7e83de0db659697eea034dd2b1620ff26ac7062709b60d723307a68aa5d81")
  set("sha256sum_3.9" "004683810c0e0b4ff10025392ac95e699e99d8c3566f415aa7fa35c6d4882f88")
  set("sha256sum_3.10" "b02692c7905dea2829e4204eab2343b226f0c9f244df89502ba8d483d5f8f9d3")
  set("sha256sum_3.11" "2a8393087e0cac9e3c0eeb69a3a34f3b01734266a0bb276621aec7a159b576b6")
  set("sha256sum_3.12" "782f1b9db7e8ff78c928ea94861549820c8abc70cde76a3dfb7ef9e54a06e326")
else ()
  message(FATAL_ERROR
    "Unknown platform for Python")
endif ()
set(filename "${python_subdir}.zip")

set(sha256sum "${sha256sum_${python_version}}")
if (NOT sha256sum)
  message(FATAL_ERROR
    "Unknown hash for ${filename}")
endif ()

# Download the file.
file(DOWNLOAD
  "${python_url_root}/${filename}"
  ".gitlab/${filename}"
  STATUS download_status
  EXPECTED_HASH "SHA256=${sha256sum}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download ${filename}: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf "${filename}"
  WORKING_DIRECTORY ".gitlab"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract ${filename}: ${err}")
endif ()

# Move to a predictable prefix.
file(RENAME
  ".gitlab/${python_subdir}"
  ".gitlab/python")
